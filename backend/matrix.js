const fetch = require("node-fetch");

var points = [[19.0189285, -98.242059],[19.0086503,-98.2474313],[19.0270414,-98.240132],[19.0056024,-98.2699076]];
var access_key = "pk.eyJ1IjoibGdiYW51ZWxvcyIsImEiOiJjanVlZm02ZmEwMWJ0M3lwZG43aGh5dHkwIn0.bkjOWEzJQMkfuoCv82x-Og"; // Copy here your own mapbox access key

fetch(`https://api.mapbox.com/directions-matrix/v1/mapbox/driving/${points.map(pair => `${pair[1]},${pair[0]}`).join(';')}.json?destinations=0&annotations=distance,duration&access_token=${access_key}`)
.then(res => res.json())
.then(res => console.log(JSON.stringify(res)));