const fetch = require("node-fetch");

var latitude = 19.0189285;
var longitude = -98.242059;
var access_key = "pk.eyJ1IjoibGdiYW51ZWxvcyIsImEiOiJjanVlZm02ZmEwMWJ0M3lwZG43aGh5dHkwIn0.bkjOWEzJQMkfuoCv82x-Og"; // Copy here your own mapbox access key

fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${longitude},${latitude}.json?access_token=${access_key}`)
.then(res => res.json())
.then(res => console.log(JSON.stringify(res)));