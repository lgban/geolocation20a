const fetch = require("node-fetch");

var lat1 = 19.0189285;
var long1 = -98.242059;
var lat2 = 19.0435669;
var long2 = -98.2375474;
var access_key = "pk.eyJ1IjoibGdiYW51ZWxvcyIsImEiOiJjanVlZm02ZmEwMWJ0M3lwZG43aGh5dHkwIn0.bkjOWEzJQMkfuoCv82x-Og"; // Copy here your own mapbox access key

fetch(`https://api.mapbox.com/directions/v5/mapbox/driving/${long1},${lat1};${long2},${lat2}.json?steps=true&access_token=${access_key}`)
.then(res => res.json())
.then(res => console.log(JSON.stringify(res)));