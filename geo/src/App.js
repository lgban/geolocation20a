import React, { useEffect, useState } from 'react';
import MapGL, {Marker} from 'react-map-gl';
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle';

function App() {
  var [latitude, setLatitude] = useState(0);
  var [longitude, setLongitude] = useState(0);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(position => {
      setLatitude(position.coords.latitude);
      setLongitude(position.coords.longitude);
    });
  });
  return (
    <div>
      <MapGL
        latitude={latitude} longitude={longitude}
        width="100%" height={600} zoom={14}
        mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}>
        <Marker
          latitude={latitude} longitude={longitude}
          offsetLeft={-10} offsetTop={-20}>
          <PersonPinCircleIcon/>
        </Marker>
      </MapGL>
    </div>
  );
}

export default App;